styles {
    element "CENCOR" {
        icon https://gitlab.com/cencor/governance/build-config/-/raw/main/structurizr/logoCencor.png
    }
    element "Person" {
        shape Person
        width 450
        height 300
        background #00cc66
        color #00cc66
        colour #000000
        stroke #006600
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "Library" {
        shape Folder
        width 450
        height 300
        background #c1c1c1
        color #c1c1c1
        colour #000000
        stroke #ffff99
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "Container" {
        shape Hexagon
        width 450
        height 300
        background #ffffcc
        color #ffffcc
        colour #000000
        stroke #ffff99
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "Software System" {
        shape RoundedBox
        width 450
        height 300
        background #0099ff
        color #0099ff
        colour #ffffff
        stroke #0066ff
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "WebBrowser" {
        shape WebBrowser
    }
    element "Repository" {
        shape Cylinder
        width 450
        height 300
        background #99ffcc
        color #99ffcc
        colour #000000
        stroke #000000
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "MessageBroker" {
        shape Pipe
        width 450
        height 300
        background #99ffcc
        color #99ffcc
        colour #000000
        stroke #000000
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "MessageBus" {
        shape Pipe
        width 450
        height 300
        background #99ffcc
        color #99ffcc
        colour #000000
        stroke #000000
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }
    element "EventBus" {
        shape Pipe
        width 450
        height 300
        background #99ffcc
        color #99ffcc
        colour #000000
        stroke #000000
        fontSize 24
        border solid
        opacity 100
        metadata true
        description false
    }

    # Estilo por default para relaciones
    relationship "Relationship" {
        dashed false
    }
    # Estilo para relaciones síncronas
    relationship "RPC" {
        color #000000
        colour #000000
        dashed false
        width 400
    }
    relationship "Sync" {
        color #000000
        colour #000000
        dashed false
        width 400
    }
    # Estilo para relaciones asincronas
    relationship "Async" {
        color #0099ff
        colour #0099ff
        dashed true
        width 400
    }
    relationship "Message" {
        color #0099ff
        colour #0099ff
        dashed true
        width 400
    }
    relationship "Event" {
        color #0099ff
        colour #0099ff
        dashed true
        width 400
    }

}

branding {
    logo https://gitlab.com/cencor/governance/build-config/-/raw/main/structurizr/logoCencor.png
}

